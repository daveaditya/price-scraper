import json
import os

from references import Partner


def load(save_file: str, partner: Partner) -> int:
    """
    Loads the progress from the save_file
    :param save_file: The name of file which contains saved data
    :param partner: The name of the partner whose data is to be loaded
    :return:
    """

    # Checks for the existence of file in the current
    # working directory
    if not os.path.exists(save_file):
        print('Missing save file...starting from 0')
        return 0

    # Opens the file and loads the saved data
    with open(save_file, 'r') as saved_data:
        # Open the JSON file
        saved = json.load(saved_data)

        # Gets the last searched product for Amazon
        data_from_file = saved[partner.value]['LastSearchId']

    # Return the obtained data
    return data_from_file


def save(save_file: str, partner: Partner, data: dict):
    """
    Saves the current progress into the save_file
    :param save_file: Name of file to which progress is to be saved
    :param partner: Name of the partner whose data is to be saved
    :param data: Information to be saved into the file
    :return: True if done successfully, else return False
    """

    # Checks for the existence of file in the current
    # working directory, if it does overwrite with new data
    if os.path.exists(save_file):

        # Opens and loads the data into the old_data object
        with open(save_file, 'r') as saved_data:
            old_data = json.load(saved_data)

        # If an old save file already exists delete
        if os.path.exists(save_file + '.old'):
            os.remove(save_file + '.old')

        # Rename the old data file
        os.rename(save_file, save_file + '.old')

        # Update the value of
        old_data[partner.value]['LastSearchId'] = data[partner.value]['LastSearchId']

        # Save the updated data to file
        with open(save_file, 'w') as to_save:
            json.dump(old_data, to_save, indent=2)
    # Else create a new file from scratch with updated data
    else:
        # Structure of the save file
        new_data = """
{
  "__comment__": {
    "LastInsertPriceId": "The ID of last inserted price tuple",
    "Amazon": "The last product searched using amazon scrapper",
    "eBay": "The last product searched using eBay scrapper",
    "Hardwire": "The last product searched using Hardwire scrapper",
    "Primeabgb": "The last product searched using Primeabgb scrapper",
    "The IT Depot": "The last product searched using The IT Depot scrapper",
    "TheITWares": "The last product searched using TheITWares scrapper"
  },
  "LastInsertPriceId": 0,
  "Amazon": {
    "LastSearchId": 0
  },
  "eBay": {
    "LastSearchId": 0
  },
  "Hardwire": {
    "LastSearchId": 0
  },
  "Primeabgb": {
    "LastSearchId": 0
  },
  "The IT Depot": {
    "LastSearchId": 0
  },
  "TheITWares": {
    "LastSearchId": 0
  }
}
        """

        # Converts string to JSON object
        new_data = json.loads(new_data)

        # Update the value of Partner's LastSearchId
        new_data[partner.value]['LastSearchId'] = data[partner.value]['LastSearchId']

        # Save the data to file
        with open(save_file, 'w') as to_save:
            json.dump(new_data, to_save, indent=2)
