import json
import mysql.connector as mysql

from os import path
from typing import List, Dict
from references import Component, Company, Partner
from mysql.connector import MySQLConnection


def get_mysql_connection(db_secret_file: str) -> MySQLConnection:
    """
    Returns the Amazon credentials from the file amaze_secret.data in a List
    :return: AmazonAPI
    """

    # Checks for the existence of file in the current
    # working directory
    if not path.exists(db_secret_file):
        print('Missing Database credentials file...')
        exit(1)

    # Reads the credentials JSON file, and extracts details for Amazon connection
    with open(db_secret_file, 'r') as db_secret:
        data = json.load(db_secret)
        mysql_secret = data['mysql']

        # Creates an AmazonAPI object with the obtained credentials
        mysql_connection = mysql.connect(**mysql_secret)

        # Returns the MySQLConnection object
        return mysql_connection


def get_model_batch(last_search_id: int, batch_size: int) -> List[Dict]:
    """
    Fetches model from product_data table, and returns a list containing them
    :param last_search_id Return the tuples after this product id
    :param batch_size The number of tuple to return at once
    :return:
    """
    # Connection to MySQL server
    db_connect = get_mysql_connection('credentials.json')

    # Gets a cursor to interact with the database
    cursor = db_connect.cursor(dictionary=True)

    # Fetches tuple from product_data
    cursor.execute(
        """
          SELECT * FROM product_data ORDER BY product_data_id LIMIT %s, %s;
        """, (last_search_id, batch_size)
    )

    # Gets an array of all the returned data
    rows = cursor.fetchall()

    # Close the cursor after use
    cursor.close()

    # Close the connection to the MySQL server
    db_connect.close()

    # Return the generator of models
    return rows


def get_all_models(last_search_id: int, batch_size: int) -> List[Dict]:
    """
    A functions that returns product_data, in a batch of given size
    :param last_search_id: ID of the product that was successfully accessed last time
    :param batch_size: Number of elements to return at once
    :return: A list of product_data tuples
    """

    # Performs the first query
    models = get_model_batch(last_search_id, batch_size)

    # Until there are no more tuple get next batch of tuples
    while True:
        # If the models are obtained, return it
        if models:

            # Increment the last search id with the obtained batch size
            last_search_id = models[-1]['product_data_id']

            yield models

            # Fetch the next batch of tuples
            models = get_model_batch(last_search_id, batch_size)

        # If the result is empty, break out of loop
        else:
            return False


def get_price_batch(last_search_id: int, batch_size: int, partner: Partner) -> List[Dict]:
    """
    Fetches price tuples from prices table, and returns a list containing them
    :param last_search_id: Return the tuples after this price id
    :param batch_size: The number of tuple to return at once
    :param partner: Name of the partner whose tuples are required
    :return:
    """
    # Connection to MySQL server
    db_connect = get_mysql_connection('credentials.json')

    # Gets a cursor to interact with the database
    cursor = db_connect.cursor(dictionary=True)

    # Fetches tuple from prices
    cursor.execute(
        """
          SELECT * FROM prices WHERE partner_name = %s AND link IS NOT NULL ORDER BY price_id LIMIT %s, %s;
        """, (partner.value, last_search_id, batch_size)
    )

    # Gets an array of all the returned data
    rows = cursor.fetchall()

    # Close the cursor after use
    cursor.close()

    # Close the connection to the MySQL server
    db_connect.close()

    # Return the generator of models
    return rows


def get_all_prices(last_search_id: int, partner: Partner, batch_size: int = 10) -> List[Dict]:
    """
    A functions that returns prices, in a batch of given size
    :param last_search_id: ID of the product that was successfully accessed last time
    :param partner: Name of the partner whose price tuples are to be fetched
    :param batch_size: Number of elements to return at once
    :return: A list of product_data tuples
    """

    # Performs the first query
    prices = get_price_batch(last_search_id, batch_size, partner)

    # Until there are no more tuple get next batch of tuples
    while True:
        # If the models are obtained, return it
        if prices:

            # Increment the last search id with the obtained batch size
            last_search_id = prices[-1]['price_id']

            yield prices

            # Fetch the next batch of tuples
            prices = get_price_batch(last_search_id, batch_size, partner)

        # If the result is empty, break out of loop
        else:
            return False


def insert_product_price(company_name: Company, component_name: Component, model: str, search_term: str,
                         partner_name: Partner, price: float, link: str, got_time: str, next_check_time: str,
                         site_specific_id: str) -> bool:
    """
    Inserts the provided data into the price table.
    :param company_name: Name of the product's company
    :param component_name: Category of the product
    :param model: Model name of the product
    :param search_term: The term that might be useful to fetch the product again
    :param partner_name: Name of the partner
    :param price: Price of the product
    :param link: Link of the partner's product page
    :param got_time: Timestamp when the product's price was obtained
    :param next_check_time: Timestamp, when the next check should occur
    :param site_specific_id: Identity of the product, that helps in quick access of product
    :return: True if data is entered successfully, else False
    """

    # Gets a MySQLConnection object with the specified credentials
    connection = get_mysql_connection('credentials.json')

    # Gets a MySQLCursor from the connection
    cursor = connection.cursor()

    # Insert the tuple into the prices table
    cursor.execute('''
            INSERT INTO prices(company_name, component_name, model, search_term, partner_name, price, link, got_time,
            next_check_time, site_specific_id) VALUES (%s, %s, %s, %s, %s, %s, %s , %s, %s, %s)
            ''', (company_name.value, component_name.value, model, search_term, partner_name.value, price, link,
                  got_time, next_check_time, site_specific_id)
    )

    # Commit the insert operation
    connection.commit()

    # If there are any warning return False
    if connection.get_warnings:
        return False

    # Else return True
    return True


def update_price(prices_id: int, price: float) -> bool:
    """
    Updates the price of given price_id tuple with the entered price
    :param prices_id: ID of tuple whose price is to be changed
    :param price: New Price to be used for the tuple
    :return: True if data is updated successfully, else False
    """

    # Gets a MySQLConnection object with the specified credentials
    connection = get_mysql_connection('credentials.json')

    # Gets a MySQLCursor from the connection
    cursor = connection.cursor()

    # Update price of the given model
    cursor.execute('''
            UPDATE prices SET price = %s WHERE price_id = %s
            ''', (price, prices_id)
    )

    # Commit the update operation
    connection.commit()

    # If there are any warning return False
    if connection.get_warnings:
        return False

    # Else return True
    return True
