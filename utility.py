import datetime
import re

from bs4 import BeautifulSoup
from datetime import timedelta
from time import time


def beautiful_xml(text):
    """
    Returns a BeautifulSoup object made with XML parser
    :param text: XML data
    :return BeautifulSoup: BeautifulSoup object with text set
    """
    return BeautifulSoup(text, 'xml')


def get_mysql_timestamp() -> str:
    """
    Returns the current timestamp in MySQL format: YYYY-MM-DD HH:MM:SS
    :return:
    """
    # Create a MySQL compatible timestamp from the current time
    return datetime.datetime.fromtimestamp(time()).strftime('%Y-%m-%d %H:%M:%S')


def add_times_and_get_timestamp(base_time: datetime, **kwargs):
    """
    Adds two times and returns MySQL compatible timestamp
    :param base_time: Base time to which new time data is to be added
    :param kwargs: Details of the new time. For permitted kwargs refer below
    :return: MySQL compatible timestamp

    : permitted kwargs:days, seconds, microseconds, milliseconds, minutes, hours, weeks
    This are the arguments to a timedelta object
    """

    # Add given base time with new time details
    new_time = base_time + timedelta(**kwargs)

    # Create a MySQL compatible timestamp from the newly obtained time
    return datetime.datetime.fromtimestamp(new_time.timestamp()).strftime('%Y-%m-%d %H:%M:%S')


def remove_extra_spaces(string: str) -> str:
    """
    Removes all the extra whitespaces from input string and
    returns the new string
    :param string: A String with extra whitespaces
    :return: A String with extra whitespaces removed
    """
    return re.sub('\s+', ' ', string)


def remove_unwanted_symbols(string: str, symbols: list) -> str:
    """
    Removes the specified symbols from the input string
    :param string: String containing some unwanted symbols
    :param symbols: A List of symbols that are to be removed from the string
    :return:
    """

    # Create a pattern with all the symbols as an OR
    symbols = r'|'.join(symbols) + '|\s+'

    # Remove the symbols and all the extra spaces if any and return the new string
    return remove_extra_spaces(re.sub(symbols, ' ', string))


def trim_unit(string: str, unit: str) -> str:
    """
    Removes any kind of unit from the end of the string
    :param string: A String containing unit at end
    :param unit: Unit to be removed from the string
    :return:
    """

    # Replace unit with a blank character and return the new string
    return string.replace(unit, '').strip()


def make_csv_line(list_of_string: list([str]), add_new_line=True) -> str:
    """
    Creates a CSV line from the list of strings
    :param list_of_string: A list of strings to create CSV from
    :param add_new_line: Whether to add new line at end or not
    :return:
    """

    # A string variable to hold the result
    result = ''

    # For every string in the list, append to the result with a comma at end
    for string in list_of_string:
        result += '"{}",'.format(string)

    # If a new line is to be added remove the trailing comma and append a new line
    # And return the new CSV string
    if add_new_line:
        return result[0: len(result) - 1] + '\r\n'
    # Else just remove the trailing comma and return the string
    else:
        return result[0: len(result) - 1]


def unmake_csv_line(line: str) -> list([str]):
    """
    Converts a CSV line into a list of strings
    :param line: A CSV line
    :return: List of strings
    """

    # Container for the result
    words = list()

    # Split the string on '","', and remove extra '"'s
    # and add to the result object
    for word in line.split('","'):
        words.append(word.strip('"'))

    # Return the generated list
    return words


def wrap_in_quotes(list_of_string: list([str])) -> str:
    """
    Concatenates the user entered string with comma in between and
    wraps that string in between double quotes
    :param list_of_string: A list of strings to wrap
    :return: A string containing all the entered string, surrounded with double quotes
    """

    # Container for result, has a starting double quote
    result = '"'

    # Separate every string of list with a comma in between
    for string in list_of_string:
        result += '{}, '.format(string)

    # Remove trailing comma and space
    result = result.rstrip(', ')

    # Append the remaining double quote
    result += '"'

    # Return the final string
    return result


def int_from_string(string: str, max_digits: int=None, qty: int=None) -> list([int]):
    """
    Extract an integer from the entered string
    :param string: A string containing integer, that needs to be extracted
    :param max_digits: The max number of digits an integer might contain
    :param qty: The number of integers that need to be extracted
    :return: A list of ints extracted from the string
    """

    # If max_digits is not specified, the max number of digits
    # can be any
    if max_digits is None:
        regex = r"\d{1,}"
    # Else if specified use it
    else:
        regex = r"\d{1," + re.escape(str(max_digits)) + r"}"

    # Gets all the numbers from the string
    result = re.findall(regex, string)

    # Converts the every number to int type
    ints = [int(num) for num in result]

    # If no quantity is specified return all ints
    if qty is None:
        return ints
    # Else return only the given number of items in list
    else:
        return ints[0:qty]


def real_from_string(string, qty=None) -> list([float]):
    """
    Extract an integer from the entered string
    :param string: A string containing float, that needs to be extracted
    :param qty: The number of integers that need to be extracted
    :return: A list of float extracted from the string
    """

    # Pattern to extract real numbers from the string
    regex = r"\d{1,}\.\d*"

    # Gets all the real numbers from the string
    result = re.findall(regex, string)

    # Converts the every number to float type
    floats = [float(num) for num in result]

    # If no quantity is specified return all floats
    if qty is None:
        return floats
    # Else return only the given number of items in list
    else:
        return floats[0:qty]


def ask_boolean_question(question: str) -> bool:
    """
    Asks user the specified question, and return True or False
    based on the user's response
    :param question: Question to prompt the user
    :return: False if user enter 'n' OR 'N', else True
    """
    # Asks the user given question and waits for response
    print('{} (y/n): '.format(question), end='')

    # Until the user enters a correct choice ask for input
    while True:
        choice = input().strip('').lower()
        # If the user enters no, return False
        if choice == 'n':
            return False
        # Else If the user enters yes, return True
        elif choice == 'y':
            return True
        # Else try again
        else:
            return ask_boolean_question(question + ' TRY AGAIN!!!')


def ask_for_data(message: str) -> str:
    """
    Asks the user to enter some data, and returns the user input
    after removing any trailing spaces
    :param message: Message to prompt the user to give input
    :return: Returns the user entered string
    """
    # Prints the message to display and waits for user response
    print('{}: '.format(message), end='')

    # Strip extra whitespaces and store user input
    data = input().strip()

    # Return the user input
    return data


def is_space_separated_numbers(string: str) -> bool:
    """
    Checks whether the entered string is composed of space separated numbers or not
    :param string: A string to test
    :return: True is the string is composed of space separated numbers, else return False
    """

    # A regex to perform the test
    pattern = re.compile(r'^\d+(\s\d+)*$')

    # Return True if any match is found
    if pattern.match(string):
        return True

    # Else return False
    return False


def get_price_from_formatted_string(string: str) -> float:
    """
    Returns the price as float from a formatted string like
    $ 49,99 OR $49,99.52 ETC.
    :param string: Price formatted string
    :return:
    """
    if string == '':
        return 0.0

    # Old pattern that had trouble with strings like 'Rs. 1599.66' i.e. 'Rs.'
    # pattern = re.compile(r'[\d+,?.?]+')

    pattern = re.compile(r'[\d+,?.?]+$')
    return float(pattern.findall(string)[0].replace(',', ''))
