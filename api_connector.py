import json

from os import path
from time import sleep
from urllib.error import HTTPError
from requests.exceptions import ReadTimeout
from bottlenose.api import Amazon
from bs4 import BeautifulSoup

from ebaysdk.finding import Connection as Finding
from ebaysdk.shopping import Connection as Shopping
from ebaysdk.response import Response as eBayResponse

from utility import beautiful_xml


def get_amazon_connection(amazon_secret_file: str) -> Amazon:
    """
    Extracts the Amazon credentials from amaze_secret_file and return an Amazon API object
    :param amazon_secret_file:
    :return:
    """

    """
    Checks for the existence of file in the current
    working directory
    """
    if not path.exists(amazon_secret_file):
        print('Missing Amazon credentials file...')
        exit(1)

    """
    Reads the credentials JSON file, and extracts details for creating an Amazon API object
    """
    with open(amazon_secret_file, 'r') as amaze_secret:
        data = json.load(amaze_secret)
        amazon_pass = data['amazon']

        """
        Creates an AmazonAPI object with the obtained credentials
        """
        amazon = Amazon(**amazon_pass, Parser=beautiful_xml)

        """
        Returns the AmazonAPI object
        """
        return amazon


def amazon_item_search(amazon: Amazon, **query) -> BeautifulSoup:
    """
    Performs an Amazon ItemSearch operation using the provided arguments
    :param amazon: An Amazon API object
    :param query: kwargs containing the parameters for the ItemSearch operation
    :return: A BeautifulSoup object containing the response from Amazon
    """

    try:
        # Search for the given model in the Amazon database
        return amazon.ItemSearch(**query)
    except HTTPError as exc:
        # An HTTPError occurs when the request rate exceeds the permitted limit
        # Hence wait for a second and send a request again
        if exc.status == 503:
            sleep(1)
            return amazon_item_search(amazon, **query)


def amazon_item_lookup(amazon: Amazon, **query) -> BeautifulSoup:
    """
    Performs an Amazon ItemLookup operation using the provided arguments
    :param amazon: An Amazon API object
    :param query: kwargs containing the parameters for the ItemLookup operation
    :return: A BeautifulSoup object containing the response from Amazon
    """

    try:
        # Search for the given model in the Amazon database
        # Searches in the 'Electronics' category, and also retrieves the 'ItemAttributes'
        return amazon.ItemLookup(**query)
    except HTTPError as exc:
        # An HTTPError occurs when the request rate exceeds the permitted limit
        # Hence wait for a second and send a request again
        if exc.status == 503:
            sleep(1)
            return amazon_item_lookup(amazon, **query)


def get_ebay_finder(ebay_secret_file: str) -> Finding:
    """
    Extracts the eBay credentials from ebay_secret_file and returns a Finding API object
    :param ebay_secret_file:
    :return:
    """

    """
    Checks for the existence of file in the current
    working directory
    """
    if not path.exists(ebay_secret_file):
        print('Missing eBay credentials file...')
        exit(1)

    """
    Reads the credentials JSON file, and extracts details for a Finding API object
    """
    with open(ebay_secret_file, 'r') as ebay_secret:
        data = json.load(ebay_secret)
        ebay_pass = data['ebay']

        """
        Creates an AmazonAPI object with the obtained credentials
        """
        finding = Finding(**ebay_pass)

        """
        Returns the AmazonAPI object
        """
        return finding


def get_ebay_shopper(ebay_secret_file: str) -> Shopping:
    """
    Extracts the eBay credentials from the file ebay_secret_file and returns a Shopping API object
    :param ebay_secret_file:
    :return:
    """

    """
    Checks for the existence of file in the current
    working directory
    """
    if not path.exists(ebay_secret_file):
        print('Missing eBay credentials file...')
        exit(1)

    """
    Reads the credentials JSON file, and extracts details for a Shopping API object
    """
    with open(ebay_secret_file, 'r') as ebay_secret:
        data = json.load(ebay_secret)
        ebay_pass = data['ebay']

        """
        Creates an AmazonAPI object with the obtained credentials
        """
        shopping = Shopping(**ebay_pass)

        """
        Returns the AmazonAPI object
        """
        return shopping


def ebay_item_search(finder: Finding, operation: str, query:dict) -> eBayResponse:
    """
    Performs a search using the eBay's Finding api
    :param finder: eBay's Finding API connection object
    :param operation: Finding API function that is to be executed
    :param query: Parameters that are needed to be execute the specified operation
    :return: Response from the Finding API
    """

    try:
        # Search for the given model in the eBay database
        response = finder.execute(operation, query)

        # If the request execution is successful return the response
        if response.reply.ack == 'Success':
            return response

    except ReadTimeout:
        # An ReadTimeout occurs when the request takes more than permitted time
        # Hence wait for a second and send a request again
        sleep(1)
        return ebay_item_search(finder, operation, query)


def ebay_price_search(shopper: Shopping, operation: str, query: dict) -> eBayResponse:
    """
    Performs a search using the eBay's Shopping api
    :param shopper: eBay's Shopping API connection object
    :param operation: Shopping API function that is to be executed
    :param query: Parameters that are needed to execute the specified operation
    :return: Response from the Shopping API
    """
    try:
        # Search for the given model in the eBay database
        response = shopper.execute(operation, query)

        # If the request execution is successful return the response
        if response.reply.ack == 'Success':
            return response

    except ReadTimeout:
        # An ReadTimeout occurs when the request takes more than permitted time
        # Hence wait for a second and send a request again
        sleep(1)
        return ebay_item_search(shopper, operation, query)
