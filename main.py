import argparse
import os
import sys

from amazon_scrapper import main as amazon_start
from ebay_scrapper import main as ebay_start
from hardwire_scrapper import main as hardwire_start
from primeabgb_scrapper import main as primeabgb_start
from theitdepot_scrapper import main as theitdepot_start
from theitwares_scrapper import main as theitwares_start

from utility import ask_boolean_question


def main():
    """
    Main starting point of the program
    :return:
    """
    # Create a new Argument Parser
    arg_parser = argparse.ArgumentParser(description='Price Scrapper')

    # An argument to specify the number of tuples that might be selected at once from the product_data table
    arg_parser.add_argument('--batch_size', default=25, type=int, choices=range(25, 100), help='Specifies the number of'
                                                                                               ' tuples that will be '
                                                                                               'retrieved at given time'
                            )

    # An argument to specify whether to resume work from last session or not. Default is False.
    arg_parser.add_argument('--resume', action='store_true', help='Specifies whether to continue from where the last'
                                                                  ' session left off or start new. Default is new '
                                                                  'session')

    # An argument to specify whether to auto save work or not. Default is True. Save is performed after every 10 tuples
    arg_parser.add_argument('--auto_save_after', action='store', default=10, type=int, choices=range(25, 100),
                            help='Specifies whether to save the progress periodically or not. Default is True')

    # An argument to specify that only the prices based on the identifier stores in prices table
    arg_parser.add_argument('--update_prices', action='store_true',
                            help='Specifies that only prices are needed to be queried. --batch_size argument is not'
                                 ' important')

    # Converts argument strings to objects
    args = arg_parser.parse_args()

    # If the system using scrapper is windows
    # Run the chcp 65001 command to change the encoding to utf-8
    if sys.platform == 'win32':
        os.system('chcp 65001')

    # Asks the user which scrapper to use, initiates the selected one
    # After exit from the scrapper again asks which one to use, or exit the product
    while True:

        print('\n' + '*' * 100)
        print('\nAvailable scrappers...')
        print('\n' + '*'*100)
        print('\n1. Amazon Scrapper')
        print('2. eBay Scrapper')
        print('3. Hardwire Scrapper')
        print('4. Primeabgb Scrapper')
        print('5. TheITDepot Scrapper')
        print('6. TheITWares Scrapper')
        print('\n' + '*'*100)
        print('\nEnter "exit" to terminate.')
        print('\n' + '*'*100)
        print('\nWhich scrapper do you want to use?: ', end='')

        choice = input()
        if choice == '1':
            print('\n\n' + '*'*100)
            print('\nUsing Amazon Scrapper...')
            amazon_start(args)
        elif choice == '2':
            print('\n\n' + '*'*100)
            print('\nUsing eBay Scrapper...')
            ebay_start(args)
        elif choice == '3':
            print('\n\n' + '*'*100)
            print('\nUsing Hardwire Scrapper...')
            hardwire_start(args)
        elif choice == '4':
            print('\n\n' + '*'*100)
            print('\nUsing Primeabgb Scrapper...')
            primeabgb_start(args)
        elif choice == '5':
            print('\n\n' + '*'*100)
            print('\nUsing TheITDepot Scrapper...')
            theitdepot_start(args)
        elif choice == '6':
            print('\n\n' + '*'*100)
            print('\nUsing TheITWares Scrapper...')
            theitwares_start(args)
        elif choice.lower() == 'exit':
            print('\n\n' + '*'*100)
            print('\nHad great fun scrapping. Good Bye!')
            break
        else:
            print('\n\n' + '*'*100)
            print('\nTry Again!!!\nWhich scrapper do you want to use?: ', end='')

        print('\n\n' + '*'*100)
        if not ask_boolean_question('\n\nWant to continue scrapping?: '):
            print('\n\n' + '$'*100)
            print('\nHad great fun scrapping. Good Bye!')
            break


if __name__ == '__main__':
    main()
