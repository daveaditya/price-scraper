import argparse
import copy
import pyperclip

from bs4 import BeautifulSoup
from datetime import datetime
from typing import List, Union, Dict

from references import Company, Component, Partner
from load_save import load, save
from db_connector import get_all_models, get_all_prices, insert_product_price, update_price
from utility import get_mysql_timestamp, add_times_and_get_timestamp, ask_boolean_question, ask_for_data,\
    is_space_separated_numbers, get_price_from_formatted_string
from website_utils import primeabgb_item_search, primeabgb_get_constructed_url, go_to_page

# A container for to store data from Primeabgb's search result
primeabgb_single_item = dict()
primeabgb_single_item['Title'] = None
primeabgb_single_item['DetailPageURL'] = None
primeabgb_single_item['Price'] = None
primeabgb_single_item['SKU'] = None
primeabgb_single_item['GotTime'] = None
primeabgb_single_item['NextCheckTime'] = None

# A container for data from Primeabgb's product's web page
primeabgb_price_item = dict()
primeabgb_price_item['PriceId'] = None
primeabgb_price_item['Title'] = None
primeabgb_price_item['URL'] = None
primeabgb_price_item['Amount'] = None

# Name of the partner from where data is scrapped
partner_name = Partner('Primeabgb')

# Stores the last product_data ID used by Primeabgb
last_search_id = 0

# A CSS selector to be used for fetching all the products from the search result of primeabgb
SELECTOR_FOR_TABLE_OF_PRODUCTS = 'div.content-product'

# A CSS selector to get the anchor tag containing URL and title
SELECTOR_FOR_LINK = 'div.product-title > a'

# A CSS selector to get the span tag containing price of the product
SELECTOR_FOR_PRICE_FROM_RESULT = 'span.price span'

# A CSS selector to get the product's title, from the product's web page
SELECTOR_FOR_PRODUCT_PAGE_TITLE = 'h1[class="title"]'

# A CSS selector to get the product's price, from the product's web page
SELECTOR_FOR_PRODUCT_PAGE_PRICE = 'div[itemprop="offers"] > meta[itemprop="price"]'

# A CSS selector to get the empty result heading tag
SELECTOR_FOR_EMPTY_RESULT = 'div.empty-category-block > h2'

# A CSS selector for the pagination block
SELECTOR_FOR_PAGINATION_BLOCK = 'nav.pagination-cubic'

# A CSS selector to get the description of total number of pages
# Example: list of 'li' containing the page numbers. Second last 'li' shows the total pages
SELECTOR_FOR_TOTAL_PAGES = 'nav.pagination-cubic li'


def get_items_from_search_result(response: str) -> Union[List[Dict], None]:
    """
    Returns the Items from the provided response
    NOTE: Used when the response contains the satisfactory product data.
    i.e. All the falls like 'No result found' and other HTTP errors are handled
    :param response: A BeautifulSoup containing the response of Primeabgb search
    :return: None if nothing is selected from obtained data. OR a list of primeabgb(s)
    """

    # Create a BeautifulSoup object from the response
    response = BeautifulSoup(response, 'lxml')

    # Gets all the products from the obtained result
    primeabgb_items = response.select(SELECTOR_FOR_TABLE_OF_PRODUCTS)

    # A container of all the primeabgb's items
    primeabgb_item_list = list()

    # Show all the titles retrieved
    for i, item in enumerate(primeabgb_items):
        # Create a new dictionary to store the item
        some = copy.copy(primeabgb_single_item)

        # Gets the <a> tag and the price from the string
        link = item.select_one(SELECTOR_FOR_LINK)
        price_span = item.select(SELECTOR_FOR_PRICE_FROM_RESULT)

        sku = item.select_one('a[data-product_sku]')['data-product_sku'].strip()

        # Used when there is discount
        if len(price_span) != 0:
            # If discount is offered select the discounted price
            if len(price_span) > 2:
                price = price_span[-2].text.strip()
            # else select the first/default price
            else:
                price = price_span[0].text.strip()
        # if 'Call for Price' is given, SELECTOR_FOR_PRICE_FROM_RESULT return None, hence skip
        else:
            print('\n\tPrice Not Found!')
            return None

        # Add item details to the dictionary
        some['Title'] = link.text.strip()
        some['DetailPageURL'] = link['href'].strip()
        some['SKU'] = sku
        some['GotTime'] = get_mysql_timestamp()
        some['NextCheckTime'] = add_times_and_get_timestamp(datetime.now(), days=1)
        some['Price'] = get_price_from_formatted_string(price)

        # Add the item to the list of retrieved items
        primeabgb_item_list.append(some)

        # Show the title and position of the current item
        print('\tPosition: {:^5}, \tTitle: {:<100}\tPrice: {:^10}'.format(i, some['Title'], some['Price']))

    # Ask the user to select the entries
    choice = ask_for_data('\n\tEnter selection')

    # Ask user input until it is not in the desired values
    while True:
        # If the choice belongs to the allowed values break out of the input loop
        if choice in ['all', 'none'] or is_space_separated_numbers(choice):
            break
        else:
            # Ask the user to try again
            choice = ask_for_data('\n\tTry Again! Enter selection')

    # If the user enters 'all', all the retrieved items are stored
    if choice == 'all':
        print('\n\tAll selected...\n')

        # Show all the selected items
        for i, item in enumerate(primeabgb_item_list):
            print('\tPosition: {:^5}, \tTitle: {:<100}'.format(i, item['Title']))

    # Else If the user enters 'none' nothing will be stored
    elif choice == 'none':
        print('\n\tNothing will be stored...')
        primeabgb_item_list = None

    # Else 'space separated numbers are entered, hence only selected items will be stored
    else:
        # Creates a list of integers from the string of space separated numbers
        positions = [int(pos) for pos in choice.split(' ')]

        # A temporary list to store all the selected items
        sub_list = list()

        # Show all the selected items
        for pos in positions:
            sub_list.append(primeabgb_item_list[pos])
            print('\n\tYou selected: {}'.format(primeabgb_item_list[pos]['Title']))

        # Save the result
        primeabgb_item_list = sub_list

    # Return the final data
    return primeabgb_item_list


def search_item(model: str) -> Union[List[Dict], None]:
    """
    Finds the item using the partner's search feature
    :param model: Name of the product to search
    :return: Returns None if nothing is obtained, else a list of primeabgb_single_item(s)
    """
    print('\nSearching for \"{}\"\n'.format(model))

    # Gets the search result using current model name
    response = primeabgb_item_search(model)

    # If there was an error in search return None
    if response is None:
        return None

    # Otherwise, create a BeautifulSoup object from the response
    soup = BeautifulSoup(response, 'lxml')

    # If the result contains an heading for empty result message return none
    if soup.select_one(SELECTOR_FOR_EMPTY_RESULT) is not None:
        # Copy the old model name to clipboard, for ease of use
        pyperclip.copy(model)

        # Ask the use for a new model name for the same product
        model = ask_for_data('\n\tPlease enter a different keyword to search')

        # If the user entered 'None', no new search is performed, hence return True
        if model == 'none':
            return None

        # Call the function with new model name
        return search_item(model)

    # If the page title contains the text 'Buy Online' means the search
    # resulted to the product page
    if soup.title.text.find('Buy Online') != -1:

        # Create a new dictionary to store the item
        product = copy.copy(primeabgb_single_item)

        # Gets the price of product
        price = soup.select_one(SELECTOR_FOR_PRODUCT_PAGE_PRICE)['content'].strip()

        # If the price says 'Call for price' return None
        # In other cases the meta[itemprop='price']['content'] has value
        if price == '0':
            print('\n\tPrice not available')
            return None

        # Collect the product's data, and store into the dictionary
        product['Title'] = soup.select_one(SELECTOR_FOR_PRODUCT_PAGE_TITLE).text.strip()
        product['DetailPageURL'] = primeabgb_get_constructed_url(model)
        product['Price'] = get_price_from_formatted_string(price)
        product['SKU'] = soup.select_one('span[itemprop="sku"]').text.strip()
        product['GotTime'] = get_mysql_timestamp()
        product['NextCheckTime'] = add_times_and_get_timestamp(datetime.now(), days=1)

        product_list = [product]

        # Show the title and position of the current item
        print('\tPosition: {:^5}, \tTitle: {:^100}, \tPrice: {:^10}'.format(0, product_list[0]['Title'],
                                                                             product['Price']))

        # Ask the user to select the entries
        choice = ask_for_data('\n\tEnter selection')

        # Ask user input until it is not in the desired values
        while True:
            # If the choice belongs to the allowed values break out of the input loop
            if choice in ['all', 'none'] or is_space_separated_numbers(choice):
                break
            else:
                # Ask the user to try again
                choice = ask_for_data('\n\tTry Again! Enter selection')

        # If the user enters 'all', all the retrieved items are stored
        if choice == 'all':
            print('\n\tAll selected...\n')

            # Show all the selected items
            for i, item in enumerate(product_list):
                print('\tPosition: {:^5}, \tTitle: {:^100}'.format(i, item['Title']))

        # Else If the user enters 'none' nothing will be stored
        elif choice == 'none':
            print('\n\tNothing will be stored...')
            product_list = None

        # Else 'space separated numbers are entered, hence only selected items will be stored
        else:
            # Creates a list of integers from the string of space separated numbers
            positions = [int(pos) for pos in choice.split(' ')]

            # A temporary list to store all the selected items
            sub_list = list()

            # Show all the selected items
            for pos in positions:
                sub_list.append(product_list[pos])
                print('\n\tYou selected: {}'.format(product_list[pos]['Title']))

            # Save the result
            product_list = sub_list

        # Return the dictionary containing the product's data, as a single element list
        return product_list

    # If there is no pagination block it means, there is only one result page
    # Hence, get the products and return
    if soup.select_one(SELECTOR_FOR_PAGINATION_BLOCK) is None:
        # Gets all the items for the page
        result = get_items_from_search_result(response)

        # Return the obtained data
        return result

    # Otherwise there are multiple pages of search result
    # Gets the total result number of pages returned for the request
    total_pages = int(soup.select(SELECTOR_FOR_TOTAL_PAGES)[-2].text.strip())

    # A container or all the returned products
    item_data_list = list()

    # If more than one page is available, for every page gets the given items
    for page in range(1, total_pages + 1):

        print('\n\tIn Page: {}'.format(page))

        # Gets the current result page using Primeabgb's search feature
        page_data = primeabgb_item_search(model, page_no=page)

        # If there was an error and no page data was recevied
        # skip the current page
        if page_data is None:
            continue

        # Gets all the items for the current page
        result = get_items_from_search_result(page_data)

        # Adds the result to the container, if something is returned
        if result is not None:
            item_data_list += result

        # Ask whether to go to next page or not, only if there
        # are more than one result pages
        if page < total_pages:
            # Ask whether to go to next page or not
            if not ask_boolean_question('\n\tWANT TO GO TO NEXT PAGE?'):
                break

    # If no items were selected, return None
    if len(item_data_list) == 0:
        return None

    # Else return the obtained data
    return item_data_list


def get_price_of_products(products: List[Dict]) -> Union[List[Dict], None]:
    """
    A function that searches for price of each product given as list, from the product's web page
    and returns a list of primeabgb_price_item(s)
    :param products: A list of identifiers to search for prices. Max number of products is 10
    :return: A list of prices from Primeabgb product's web page
    """

    # A list of product's urls
    urls = [model['link'] for model in products]

    # For every product URL, go to that page and fetch the title and price
    for url in urls:
        # Gets the page data for the current URL
        response = go_to_page(url)

        # Creates a BeautifulSoup object from the page's data
        soup = BeautifulSoup(response, 'lxml')

        # Gets the title and price of product
        title = soup.select_one(SELECTOR_FOR_PRODUCT_PAGE_TITLE).text.strip()
        price = soup.select_one(SELECTOR_FOR_PRODUCT_PAGE_PRICE)['content'].strip()

        # Gets the product with current title
        current_product = next((item for item in products if item['Title'] == title), None)

        if current_product:
            # If the 'Call For Price' is mentioned, hence value is 0 store None
            if price == '0':
                current_product['Amount'] = None
            else:
                # Gets the value of Amount tag for every item and convert to proper currency
                current_product['Amount'] = get_price_from_formatted_string(price)

    # Return the list of amounts
    return products


def update_prices():
    """
    Updates price of all the products in 'prices' table where the partner name is Primeabgb.
    10 products are queried at a single instance of time
    :return:
    """
    # Performs the search operation batch-vice
    for batch in get_all_prices(last_search_id, partner_name):

        # A container for prices for the current batch
        price_list = list()

        # For every product in the batch store the required data in primeabgb_price_item
        for product in batch:
            # Create a container for product's details
            some = copy.copy(primeabgb_price_item)

            # Hold important data of current product
            some['PriceId'] = product['price_id']
            some['Title'] = product['search_term']
            some['URL'] = product['link']
            some['Amount'] = product['price']

            # Add the product to main container for the current batch
            price_list.append(some)

        # Fetch price of all products in current batch
        price_list = get_price_of_products(price_list)

        # If prices are obtained successfully, update the database
        if price_list is not None:

            # For every product update it, with the obtained price
            for product in price_list:

                # Update current product's price
                update_price(product['PriceId'], product['Amount'])


def main(args: argparse.Namespace = None):
    """
    Starting point of the Primeabgb Price Scrapper
    :return:
    """

    # Create a new Argument Parser
    arg_parser = argparse.ArgumentParser(description='Primeabgb Price Scrapper')

    # An argument to specify the number of tuples that might be selected at once from the product_data table
    arg_parser.add_argument('--batch_size', default=25, type=int, choices=range(25, 100), help='Specifies the number of'
                                                                                               ' tuples that will be '
                                                                                               'retrieved at given time'
                            )

    # An argument to specify whether to resume work from last session or not. Default is False.
    arg_parser.add_argument('--resume', action='store_true', help='Specifies whether to continue from where the last'
                                                                  ' session left off or start new. Default is new '
                                                                  'session')

    # An argument to specify whether to auto save work or not. Default is True. Save is performed after every 10 tuples
    arg_parser.add_argument('--auto_save_after', action='store', default=10, type=int, choices=range(25, 100),
                            help='Specifies whether to save the progress periodically or not. Default is True')

    # An argument to specify that only the prices based on the identifier stores in prices table
    arg_parser.add_argument('--update_prices', action='store_true',
                            help='Specifies that only prices are needed to be queried. --batch_size argument is not'
                                 ' important')

    # If the arguments are not passed, it means debugging is being done
    # Hence accept the arguments that are passed to this particular file
    if args is None:
        # Converts argument strings to objects
        args = arg_parser.parse_args()

    # If only prices are required, run the corresponding function, and return
    if getattr(args, 'update_prices'):
        return update_prices()

    # Bring the global variable to current visibility
    global last_search_id

    # If the 'resume' argument was mentioned in the commandline, start from the previous session
    if getattr(args, 'resume'):
        # Get initial data from saved file
        last_search_id = load('savepoint.json', partner_name)

        # Prints the starting product_data ID
        print('\nStarting from ... {}'.format(last_search_id + 1))

    # Initialize counter with 0, used to track number of entries collected
    # And perform auto save accordingly
    counter = 0

    # A container to hold all retrieved data until saved
    final_data = list()

    # Performs the search operation batch-vice
    for batch in get_all_models(last_search_id, getattr(args, 'batch_size')):

        # Gets a batch of model and finds them in Primeabgb's Website
        for model in batch:

            # If after an auto save the final_data is set to None, create a new list
            if final_data is None:
                final_data = list()

            # Search for the product
            items = search_item(model['model'])

            # If even a single item was obtained find its price
            if items is not None:

                # Increment the counter by the number of items obtained
                counter += len(items)

                # Print Header line
                print('\n{:<100}\t{:^30}\t{:^10}'.format('Title', 'Model', u'Price(\u20B9)'))

                # Store all the selected items into the prices table
                for item in items:

                    # A dictionary containing all the product data, except 'price'
                    product = dict()
                    product['company_name'] = Company(model['company_name'])
                    product['component_name'] = Component(model['component_name'])
                    product['model'] = model['model']
                    product['search_term'] = item['Title']
                    product['partner_name'] = partner_name
                    product['price'] = item['Price']
                    product['link'] = item['DetailPageURL']
                    product['site_specific_id'] = item['SKU']
                    product['got_time'] = item['GotTime']
                    product['next_check_time'] = item['NextCheckTime']

                    # Add product data to final_data
                    final_data.append(product)

                    # Print the model name, title and Price of every obtained product
                    print('\n{:<100}\t{:^30}\t{:^10}'.format(product['search_term'], product['model'],
                                                              product['price']))
            else:

                # Increment the counter to denote that one item was searched and nothing was obtained
                counter += 1

            # Add the current product_data_id as last_searched
            last_search_id = model['product_data_id']

            # Perform a save operation on completion of every specified amount of entries
            if counter > getattr(args, 'auto_save_after') and len(final_data) > 0:

                # Insert every product into the price table
                for product in final_data:
                    # Insert the data into the table
                    insert_product_price(**product)

                final_data = None
                counter = 0

                # Saves the last search id into a file
                save('savepoint.json', partner_name, {partner_name.value: {"LastSearchId": last_search_id}})

            # Asks the user whether to search for more products or not
            if not ask_boolean_question('\n\nDO YOU WANT TO CONTINUE?'):

                # If the program is stopped abruptly, ask whether to save unsaved data or not
                if final_data is not None and ask_boolean_question('\nWANT TO SAVE YOUR WORK?'):

                    # Insert every product into the price table
                    for product in final_data:
                        # Insert the data into the table
                        insert_product_price(**product)

                    # Saves the last search id into a file
                    save('savepoint.json', partner_name, {partner_name.value: {"LastSearchId": last_search_id}})

                    # Exit out of main loop safely
                    return

                # Since auto save was performed, exit the main loop
                else:
                    # Exit main loop
                    return


# Only to be used for debugging
if __name__ == '__main__':
    main()
