import requests

from typing import Union

# Search URL structure for www.hardwire.in
HARDWIRE_SEARCH_URL = 'http://hardwire.in/search?controller=search&orderby=position&orderway=dsc&search_cat_select=0' \
             '&search_query={search_query}&submit_search=&n={results_per_page}'

# Search URL structure for www.primeabgb.com
PRIMEABGB_SEARCH_URL = 'http://www.primeabgb.com/?s={search_query}&post_type=product'

# Pagination URL structure for www.primeabgb.com
PRIMEABGB_SEARCH_PAGE_URL = 'http://www.primeabgb.com/page/{page}/?s={search_query}&post_type=product'

# Search URL for www.theitdepot.com
THEITDEPOT_SEARCH_URL = 'https://www.theitdepot.com/search_filter.php'

# Search URL structure for www.theitwares.com
THEITWARES_SEARCH_URL = 'http://www.theitwares.com/index.php?route=product/search&from=normal&' \
                        'search={search_query}&description=true&limit={results_per_page}'


def go_to_page(url: str) -> Union[str, None]:
    """
    A function that returns the data of web page denoted by the entered URL.
    :param url: URL of web page whose data is to be returned
    :return: Contents of the web page denoted by URL
    """

    try:
        # Gets the entered page
        response = requests.get(url)

        # Raises an exception with HTTP response code is not 200
        response.raise_for_status()
    # Handle the HTTPError exception
    except requests.exceptions.HTTPError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handle the ConnectionError exception
    except requests.exceptions.ConnectionError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handles the RequestException, which is a broader exception for
    # all the exceptions raised by the requests API
    except requests.exceptions.RequestException as exc:
        print('\n\nSome exception : ' + exc.request)
        return None

    # Return the web page's data
    return response.text


def hardwire_item_search(query: str, per_page: int = 15, page_no: int = 0) -> Union[str, None]:
    """
    A function that performs search query for the Hardwire's website and returns the obtained data.
    :param query: The term to search in the Hardwire's website
    :param per_page: Number of products to fetch at once
    :param page_no: Page number of the search result to be fetched
    :return: Contents of the web page obtained after performing search operation
    """

    # Replace all spaces with '+' to make it compatible for searching
    query = query.strip().replace(' ', '+')

    # Create a copy of the hardwire's search URL
    url = HARDWIRE_SEARCH_URL

    try:
        # if page number is given, add the '&p=' argument to the url
        if page_no != 0:
            url += '&p={page}'.format(page=page_no)

        # Perform an search using the given model, and other parameters
        response = requests.get(url.format(search_query=query, results_per_page=per_page))

        # Raises an exception with HTTP response code is not 200
        response.raise_for_status()
    # Handle the HTTPError exception
    except requests.exceptions.HTTPError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handle the ConnectionError exception
    except requests.exceptions.ConnectionError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handles the RequestException, which is a broader exception for
    # all the exceptions raised by the requests API
    except requests.exceptions.RequestException as exc:
        print('\n\nSome exception : ' + exc.request)
        return None

    # Return the web page's data
    return response.text


def primeabgb_get_constructed_url(query: str) -> str:
    """
    A function that return the url that is used to access the Primeabgb website
    :param query: Search query to be used
    :return:
    """
    # Replace all spaces with '+' to make it compatible for searching
    query = query.strip().replace(' ', '+')

    return PRIMEABGB_SEARCH_URL.format(search_query=query)


def primeabgb_item_search(query: str, page_no: int = 0) -> Union[str, None]:
    """
    A function that performs search query for the Primeabgb's website and returns the obtained data.
    :param query: The term to search in the Primeabgb's website
    :param page_no: Page number of the search result to be fetched
    :return: Contents of the web page obtained after performing search operation
    """

    # Replace all spaces with '+' to make it compatible for searching
    query = query.strip().replace(' ', '+')

    try:
        # Add the query to url
        url = PRIMEABGB_SEARCH_URL.format(search_query=query)

        # If a page number is specified use PRIMEABGB_SEARCH_PAGE_URL, and set the page no and query
        if page_no != 0:
            url = PRIMEABGB_SEARCH_PAGE_URL.format(page=page_no, search_query=query)

        # Perform an search using the given model
        response = requests.get(url)

        # Raises an exception with HTTP response code is not 200
        response.raise_for_status()
    # Handle the HTTPError exception
    except requests.exceptions.HTTPError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handle the ConnectionError exception
    except requests.exceptions.ConnectionError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handles the RequestException, which is a broader exception for
    # all the exceptions raised by the requests API
    except requests.exceptions.RequestException as exc:
        print('\n\nSome exception : ' + exc.request)
        return None

    # Return the web page's data
    return response.text


def theitdepot_item_search(query: str, page_no: int = 0) -> Union[str, None]:
    """
    A function that performs search query for the TheITDepot's website and returns the obtained data.
    :param query: The term to search in the Hardwire's website
    :param page_no: Page number of the search result to be fetched
    :return: Contents of the web page obtained after performing search operation
    """

    #
    headers = {
        'Host': 'www.theitdepot.com',
        'X-Requested-With': 'XMLHttpRequest'
    }

    #
    data = {
        'search_keywords': query,
        'filter-limit': 12,
        'filter-orderby': 'price-asc',
        'filter_listby': 'Grid',
        'filter': 'true'
    }

    try:
        # if page number is given, add the '&p=' argument to the url
        if page_no != 0:
            data['pageno'] = page_no

        # Perform an search using the given model, and other parameters
        response = requests.post(THEITDEPOT_SEARCH_URL, data=data, headers=headers)

        # Raises an exception with HTTP response code is not 200
        response.raise_for_status()
    # Handle the HTTPError exception
    except requests.exceptions.HTTPError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handle the ConnectionError exception
    except requests.exceptions.ConnectionError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handles the RequestException, which is a broader exception for
    # all the exceptions raised by the requests API
    except requests.exceptions.RequestException as exc:
        print('\n\nSome exception : ' + exc.request)
        return None

    # Return the web page's data
    return response.text


def theitwares_item_search(query: str, per_page: int = 25, page_no: int = 0) -> Union[str, None]:
    """
    A function that performs search query for the TheITWares's website and returns the obtained data.
    :param query: The term to search in the Hardwire's website
    :param per_page: Number of products to fetch at once, Permitted values are 25, 50, 75, 100
    :param page_no: Page number of the search result to be fetched
    :return: Contents of the web page obtained after performing search operation
    """

    # Create a copy of the theitwares' search URL
    url = THEITWARES_SEARCH_URL

    try:
        # if page number is given, add the '&p=' argument to the url
        if page_no != 0:
            url += '&page={page}'.format(page=page_no)

        # Perform an search using the given model, and other parameters
        response = requests.get(url.format(search_query=query, results_per_page=per_page))

        # Raises an exception with HTTP response code is not 200
        response.raise_for_status()
    # Handle the HTTPError exception
    except requests.exceptions.HTTPError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handle the ConnectionError exception
    except requests.exceptions.ConnectionError as exc:
        print('\n\nSome exception : ' + exc.request)
        return None
    # Handles the RequestException, which is a broader exception for
    # all the exceptions raised by the requests API
    except requests.exceptions.RequestException as exc:
        print('\n\nSome exception : ' + exc.request)
        return None

    # Return the web page's data
    return response.text
