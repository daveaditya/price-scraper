import argparse
import copy
import pyperclip

from datetime import datetime
from typing import List, Union, Dict

from ebaysdk.finding import Connection as Finding
from ebaysdk.shopping import Connection as Shopping

from references import Company, Component, Partner
from load_save import load, save
from api_connector import get_ebay_finder, get_ebay_shopper, ebay_item_search, ebay_price_search
from db_connector import get_all_models, get_all_prices, insert_product_price, update_price
from utility import get_mysql_timestamp, add_times_and_get_timestamp, ask_boolean_question, ask_for_data,\
    is_space_separated_numbers

# A container for to store data from eBay findItemsAdvanced
ebay_single_item = dict()
ebay_single_item['ItemId'] = None
ebay_single_item['Title'] = None
ebay_single_item['DetailPageURL'] = None
ebay_single_item['Price'] = None
ebay_single_item['GotTime'] = None
ebay_single_item['NextCheckTime'] = None

# A container for data from eBay getMultipleItems
ebay_price_item = dict()
ebay_price_item['PriceId'] = None
ebay_price_item['ItemId'] = None
ebay_price_item['Amount'] = None

# Name of the partner whose API is used
partner_name = Partner('eBay')

# Stores the last product_data ID used by eBay scrapper
last_search_id = 0


def get_items_from_response(response: dict) -> Union[List[Dict], None]:
    """
    Returns the Items from the provided response
    NOTE: Used when the response contains the satisfactory product data.
    i.e. All the falls are handled before calling this function
    :param response: A response dictionary returned by the Finding API
    :return: None if nothing is selected from obtained data. OR a list of ebay_single_item(s)
    """

    # Gets all the items from the input response
    ebay_items = response['searchResult']['item']

    # A container of all the eBay items
    ebay_item_list = list()

    # Show all the titles retrieved
    for i, item in enumerate(ebay_items):
        # Create a new dictionary to store the item
        some = copy.copy(ebay_single_item)

        # Add item details to the dictionary
        some['Title'] = item['title']
        some['ItemId'] = item['itemId']
        some['DetailPageURL'] = item['viewItemURL']
        some['GotTime'] = get_mysql_timestamp()
        some['NextCheckTime'] = add_times_and_get_timestamp(datetime.now(), days=1)
        some['Price'] = float(item['sellingStatus']['convertedCurrentPrice']['value'])

        # Add the item to the list of retrieved items
        ebay_item_list.append(some)

        # Show the title and position of the current item
        print('\tPosition: {:^5}, \tTitle: {:<100}\tPrice: {:^10}'.format(i, some['Title'], some['Price']))

    # Ask the user to select the entries
    choice = ask_for_data('\n\tEnter selection')

    # Ask user input until it is not in the desired values
    while True:
        # If the choice belongs to the allowed values break out of the input loop
        if choice in ['all', 'none'] or is_space_separated_numbers(choice):
            break
        else:
            # Ask the user to try again
            choice = ask_for_data('\n\tTry Again! Enter selection')

    # If the user enters 'all', all the retrieved items are stored
    if choice == 'all':
        print('\n\tAll selected...\n')

        # Show all the selected items
        for i, item in enumerate(ebay_item_list):
            print('\tPosition: {:^5}, \tTitle: {:<100}'.format(i, item['Title']))

    # Else If the user enters 'none' nothing will be stored
    elif choice == 'none':
        print('\n\tNothing will be stored...')
        ebay_item_list = None

    # Else 'space separated numbers are entered, hence only selected items will be stored
    else:
        # Creates a list of integers from the string of space separated numbers
        positions = [int(pos) for pos in choice.split(' ')]

        # A temporary list to store all the selected items
        sub_list = list()

        # Show all the selected items
        for pos in positions:
            sub_list.append(ebay_item_list[pos])
            print('\n\tYou selected: {}'.format(ebay_item_list[pos]['Title']))

        # Save the result
        ebay_item_list = sub_list

    # Return the final data
    return ebay_item_list


def get_items(model: str, finder: Finding) -> Union[List[Dict], None]:
    """
    Gets the product information from the eBay Finding API and returns a list of matched
    product with its details
    :param model: Name of the product to search
    :param finder: Finding API object, to perform operations with
    :return: A list of items retrieved from eBay's Finding API
    """
    print('\nSearching for \"{}\"\n'.format(model))

    # A dictionary containing arguments for the eBay's Finding API
    search_query = dict()
    search_query['keywords'] = model
    search_query['paginationInput'] = {'entriesPerPage': 20}

    # Perform an Ebay findItemsAdvanced operation
    response = ebay_item_search(finder, 'findItemsAdvanced', search_query)

    # If there is an error process it
    if response.reply.ack != 'Success':
        # A NoExactMatches error means the requested keyword does not return any value
        # Hence, get a new keyword for the same product to search for
        error = response.dict()['errorMessage']['error']
        print('\n\tAn Error occurred: {}\t{}'.format(error['errorId'], error['message']))
        return None

    # Gets the dictionary of response
    response_dict = response.dict()

    # If the number of results obtained is zero, ask for another keyword,
    # for the same model
    if response_dict['searchResult']['_count'] == '0':
        # Copy the old model name to clipboard, for ease of use
        pyperclip.copy(model)

        # Ask the use for a new model name for the same product
        model = ask_for_data('\n\tPlease enter a different keyword to search')

        # If the user entered 'None', no new search is performed, hence return True
        if model == 'none':
            return None

        # Call the function with new model name
        return get_items(model, finder)

    # Gets the total result number of pages returned for the request
    total_pages = int(response_dict['paginationOutput']['totalPages'])

    # If the number of result pages available is more than 10
    # restrict to the default limit of '10' when the
    # SearchIndex is not 'All'
    if total_pages > 100:
        stop_page = 100
    # Else the number of pages returned are all browse- able
    # and hence can be used without any restrictions
    else:
        stop_page = total_pages

    # A container or all the returned products
    item_data_list = list()

    # If only one page is available
    if total_pages == 1:
        # Gets all the items for the page
        result = get_items_from_response(response_dict)

        # Return the obtained data
        return result

    # For every page gets the given items
    for page in range(1, stop_page + 1):

        print('\n\tIn Page: {}'.format(page))

        # Add the 'ItemPage' parameter to the search
        search_query['paginationInput']['pageNumber'] = page

        # Gets the current result page using eBay's Finding API findItemsAdvanced
        page_data = ebay_item_search(finder, 'findItemsAdvanced', search_query)

        # Gets all the items for the current page, sent in form of dict()
        result = get_items_from_response(page_data.dict())

        # Adds the result to the container, if something is returned
        if result is not None:
            item_data_list += result

        # Ask whether to go to next page or not, only if there
        # are more than one result pages
        if page < stop_page:
            # Ask whether to go to next page or not
            if not ask_boolean_question('\n\tWANT TO GO TO NEXT PAGE?'):
                break

    # If no items were selected, return None
    if len(item_data_list) == 0:
        return None

    # Else return the obtained data
    return item_data_list


def get_price_using_id(products: List[Dict], shopper: Shopping) -> Union[List[Dict], None]:
    """
    A function that searches for price of each identifier given as list, from the eBay Shopping API
    and returns a list of prices
    :param products: A list of identifiers to search for prices. Max number of IDs is 10
    :param shopper: eBay Shopping API object
    :return: A list of prices from eBay
    """

    # A list of products IDs
    item_ids = [model['ItemId'] for model in products]

    # A dictionary containing arguments for the eBay Shopping API GetMultipleItem operation
    query = dict()
    query['ItemId'] = item_ids
    query['IncludeSelector'] = 'Details'

    # Perform an GetMultipleItems for the given item ids
    response = ebay_price_search(shopper, 'GetMultipleItems', query)

    # If there is an error process it
    if response.reply.ack != 'Success':
        # A NoExactMatches error means the requested keyword does not return any value
        # Hence, get a new keyword for the same product to search for
        error = response.dict()['errorMessage']['error']
        print('\n\tAn Error occurred: {}\t{}'.format(error['errorId'], error['message']))
        return None

    # Retrieve the error code if any
    response_dict = response.dict()

    # Gets a list of every items
    for item in response_dict['Item']:

        # Store the ItemId of current item
        item_id = item['ItemId']

        # For every product in the list
        for p in products:
            # If the item_id matches and price tag is present store the new price
            if p['ItemId'] == item_id:
                # Gets the value of Amount tag for every item and convert to proper currency
                p['Amount'] = float(item['CurrentPrice']['Value'])

    # Return the list of amounts
    return products


def update_prices():
    """
    Updates price of all the products in 'prices' table. 20 products are queried at a
    single instance of time
    :return:
    """
    # Connect to eBay India with given credentials and a BeautifulSoup XML parser
    ebay_shopper = get_ebay_shopper('credentials.json')

    # Performs the search operation batch-vice
    for batch in get_all_prices(last_search_id, partner_name, batch_size=20):

        # A container for prices for the current batch
        price_list = list()

        for product in batch:
            # Create a container for product's details
            some = copy.copy(ebay_price_item)

            # Hold important data of current product
            some['ItemId'] = product['site_specific_id']
            some['PriceId'] = product['price_id']
            some['Amount'] = product['price']

            # Add the product to main container for the current batch
            price_list.append(some)

        # Fetch price of all products in current batch
        price_list = get_price_using_id(price_list, ebay_shopper)

        # If prices are obtained successfully, update the database
        if price_list is not None:

            # For every product update it, with the obtained price
            for product in price_list:

                # Update current product's price
                update_price(product['PriceId'], product['Amount'])


def main(args: argparse.Namespace = None):
    """
    Starting point of the eBay Price Scrapper
    :return:
    """

    # Create a new Argument Parser
    arg_parser = argparse.ArgumentParser(description='eBay Price Scrapper')

    # An argument to specify the number of tuples that might be selected at once from the product_data table
    arg_parser.add_argument('--batch_size', default=25, type=int, choices=range(25, 100), help='Specifies the number of'
                                                                                               ' tuples that will be '
                                                                                               'retrieved at given time'
                            )

    # An argument to specify whether to resume work from last session or not. Default is False.
    arg_parser.add_argument('--resume', action='store_true', help='Specifies whether to continue from where the last'
                                                                  ' session left off or start new. Default is new '
                                                                  'session')

    # An argument to specify whether to auto save work or not. Default is True. Save is performed after every 10 tuples
    arg_parser.add_argument('--auto_save_after', action='store', default=10, type=int, choices=range(25, 100),
                            help='Specifies whether to save the progress periodically or not. Default is True')

    # An argument to specify that only the prices based on the identifier stores in prices table
    arg_parser.add_argument('--update_prices', action='store_true',
                            help='Specifies that only prices are needed to be queried. --batch_size argument is not'
                                 ' important')

    # If the arguments are not passed, it means debugging is being done
    # Hence accept the arguments that are passed to this particular file
    if args is None:
        # Converts argument strings to objects
        args = arg_parser.parse_args()

    # If only prices are required, run the corresponding function, and return
    if getattr(args, 'update_prices'):
        return update_prices()

    # Bring the global variable to current visibility
    global last_search_id

    # If the 'resume' argument was mentioned in the commandline, start from the previous session
    if getattr(args, 'resume'):
        # Get initial data from saved file
        last_search_id = load('savepoint.json', partner_name)

        # Prints the starting product_data ID
        print('\nStarting from ... {}'.format(last_search_id + 1))

    # Connect to eBay India with given credentials and a BeautifulSoup XML parser
    ebay_finder = get_ebay_finder('credentials.json')

    # Initialize counter with 0, used to track number of entries collected
    # And perform auto save accordingly
    counter = 0

    # A container to hold all retrieved data until saved
    final_data = list()

    # Performs the search operation batch-vice
    for batch in get_all_models(last_search_id, getattr(args, 'batch_size')):

        # Gets a batch of model and finds them in eBay's database
        for model in batch:

            # If after an auto save the final_data is set to None, create a new list
            if final_data is None:
                final_data = list()

            # Search for the product
            items = get_items(model['model'], ebay_finder)

            # If even a single item was obtained find its price
            if items is not None:

                # Increment the counter by the number of items obtained
                counter += len(items)

                # Print Header line
                print('\n{:<100}\t{:^30}\t{:^10}'.format('Title', 'ItemId', u'Price(\u20B9)'))

                # Store all the selected items into the prices table
                for item in items:

                    # A dictionary containing all the product data, except 'price'
                    product = dict()
                    product['company_name'] = Company(model['company_name'])
                    product['component_name'] = Component(model['component_name'])
                    product['model'] = model['model']
                    product['search_term'] = item['Title']
                    product['partner_name'] = partner_name
                    product['price'] = item['Price']
                    product['link'] = item['DetailPageURL']
                    product['site_specific_id'] = item['ItemId']
                    product['got_time'] = item['GotTime']
                    product['next_check_time'] = item['NextCheckTime']

                    # Add product data to final_data
                    final_data.append(product)

                    # Print the Title, ItemID and Price of every obtained product
                    print('\n{:<100}\t{:^30}\t{:^10}'.format(product['search_term'], product['site_specific_id'],
                                                            product['price']))
            else:

                # Increment the counter to denote that one item was searched and nothing was obtained
                counter += 1

            # Add the current product_data_id as last_searched
            last_search_id = model['product_data_id']

            # Perform a save operation on completion of every specified amount of entries
            if counter > getattr(args, 'auto_save_after') and len(final_data) > 0:

                # Insert every product into the price table
                for product in final_data:
                    # Insert the data into the table
                    insert_product_price(**product)

                final_data = None
                counter = 0

                # Saves the last search id into a file
                save('savepoint.json', partner_name, {partner_name.value: {"LastSearchId": last_search_id}})

            # Asks the user whether to search for more products or not
            if not ask_boolean_question('\n\nDO YOU WANT TO CONTINUE?'):

                # If the program is stopped abruptly, ask whether to save unsaved data or not
                if final_data is not None and ask_boolean_question('\nWANT TO SAVE YOUR WORK?'):

                    # Insert every product into the price table
                    for product in final_data:
                        # Insert the data into the table
                        insert_product_price(**product)

                    # Saves the last search id into a file
                    save('savepoint.json', partner_name, {partner_name.value: {"LastSearchId": last_search_id}})

                    # Exit out of main loop safely
                    return

                # Since auto save was performed, exit the main loop
                else:
                    # Exit main loop
                    return


# Only to be used for debugging
if __name__ == '__main__':
    main()
